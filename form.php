<?php
require_once 'bd.php';
?>

<!DOCTYPE html>
<html>

<head>
    <title>Ajout d'un objet</title>
    <link rel="stylesheet" href="formDeSaisie.css">
    <script>
        document.addEventListener("DOMContentLoaded", function() {
            var problemeCount = 1;

            // Écoutez l'événement click sur le bouton "Ajouter un autre problème"
            document.getElementById("ajouterProbleme").addEventListener("click", function() {
                var problemeContainer = document.getElementById("problemeContainer");

                // Créez un nouvel élément pour le problème
                var problemeDiv = document.createElement("div");

                // Créez un label pour le numéro du problème
                var numeroLabel = document.createElement("label");
                numeroLabel.textContent = "Problème " + problemeCount + ":";
                problemeDiv.appendChild(numeroLabel);

                // Incrémente le compteur de problème
                problemeCount++;

                // Créez un label pour la description du problème
                var descriptionLabel = document.createElement("label");
                descriptionLabel.textContent = "Description du problème:";
                problemeDiv.appendChild(descriptionLabel);

                // Créez un champ de saisie pour la description du problème
                var descriptionInput = document.createElement("textarea");
                descriptionInput.name = "description[]";
                descriptionInput.placeholder = "Description du problème";
                problemeDiv.appendChild(descriptionInput);

                // Créez un label pour la date du problème
                var dateProblemeLabel = document.createElement("label");
                dateProblemeLabel.textContent = "Date du problème:";
                problemeDiv.appendChild(dateProblemeLabel);

                // Créez un champ de saisie pour la date du problème
                var dateProblemeInput = document.createElement("input");
                dateProblemeInput.type = "date";
                dateProblemeInput.name = "dateProbleme[]";
                problemeDiv.appendChild(dateProblemeInput);

                // Créez un label pour la date de résolution
                var dateResolutionLabel = document.createElement("label");
                dateResolutionLabel.textContent = "Date de résolution:";
                problemeDiv.appendChild(dateResolutionLabel);

                // Créez un champ de saisie pour la date de résolution
                var dateResolutionInput = document.createElement("input");
                dateResolutionInput.type = "date";
                dateResolutionInput.name = "dateResolution[]";
                problemeDiv.appendChild(dateResolutionInput);

                // Créez un label pour la résolution du problème
                var resolutionLabel = document.createElement("label");
                resolutionLabel.textContent = "Résolution du problème:";
                problemeDiv.appendChild(resolutionLabel);

                // Créez un champ de saisie pour la résolution du problème
                var resolutionInput = document.createElement("textarea");
                resolutionInput.name = "resolution[]";
                resolutionInput.placeholder = "Résolution du problème";
                problemeDiv.appendChild(resolutionInput);

                // Ajoutez le bloc du problème au conteneur
                problemeContainer.appendChild(problemeDiv);
            });
        });
    </script>
</head>

<body>
    <?php
    // Connexion à la base de données avec PDO
    // $conn = new PDO("mysql:host=localhost;dbname=gestionParcInfo", "gestionParcInfo", "pass_gestionParcInfo");

    // Vérification du formulaire soumis
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Récupération des données du formulaire
        $nom = $_POST['nom'];
        $numero_serie = $_POST['numero_serie'];
        $site_internet = $_POST['site_internet'];
        $adresse_Ip = $_POST["adresse_Ip"];
        $domaine = $_POST["domaine"];
        $processeur = $_POST["processeur"];
        $memoire = $_POST["memoire"];
        $Disque_Dur = $_POST["Disque_Dur"];
        $carte_video = $_POST["carte_video"];
        $windows = $_POST["windows"];
        $lumen = $_POST["lumen"];
        $watt = $_POST["watt"];
        $etat = $_POST["etat"];
        $reference = $_POST["reference"];
        $factureA = $_POST["factureA"];
        $contrat = $_POST["contrat"] ? 1 : 0;
        $date_debut_engagement = $_POST["date_debut_engagement"];
        $date_fin_engagement = $_POST["date_fin_engagement"];
        $type_de_renouvellement = $_POST["type_de_renouvellement"];
        $mode_de_paiement = $_POST["mode_de_paiement"];
        $periodicite_de_facturation = $_POST["periodicite_de_facturation"];
        $date_debut_facturation = $_POST["date_debut_facturation"];
        $date_fin_facturation = $_POST["date_fin_facturation"];
        $service_localisation_id = $_POST["service_localisation"];

        // $salarie_id = $_POST["salarie"];

        if (isset($_POST["salarie"])) {
            $salarie_id = $_POST["salarie"];
        }


        $typeObjet_id = $_POST["typeObjet"];

        // Récupération des données du problème
        $description = $_POST['description'];
        $date_probleme = $_POST['dateProbleme'];
        $date_de_resolution = $_POST['dateResolution'];
        $resolution = $_POST['resolution'];


        $type_garantie = $_POST["type_garantie"];
        $date_de_debut_de_garantie = $_POST["date_de_debut_de_garantie"];
        $date_de_fin_de_garantie = $_POST["date_de_fin_de_garantie"];
        $prix_Ht_avec_garantie = $_POST["prix_Ht_avec_garantie"];
        $prix_Ttc_avec_garantie = $_POST["prix_Ttc_avec_garantie"];
        $prix_Ht_sans_garantie = $_POST["prix_Ht_sans_garantie"];
        $prix_Ttc_sans_garantie = $_POST["prix_Ttc_sans_garantie"];
        $prix_Ht_garantie = $_POST["prix_Ht_garantie"];
        $prix_Ttc_garantie = $_POST["prix_Ttc_garantie"];
        $service_localisation_id = $_POST["service_localisation"];
        // Marque
        // $nom_marque = $_POST["nom_marque"];
        if (isset($_POST["nom_marque"])) {
            $nom_marque = $_POST["nom_marque"];
        }

        $marque_id = $_POST["marque_id"];
        // projet

        $projets_id  = $_POST['projet'];

        // Requête pour récupérer le nom Projet et les date
        $query = "SELECT nom, date_debut, date_fin FROM projets WHERE id = :projets_id";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':projets_id', $projets_id);

        // // Exécution de la requête d'insertion du projet
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);



        // Salarie
        // Récupération de l'ID du salarié sélectionné

        // $salarie_id = $_POST["salarie"];
        if (isset($_POST["salarie"])) {
            $salarie_id = $_POST["salarie"];
        }



        // Requête pour récupérer le nom du salarié
        $query = "SELECT nom FROM salarie WHERE id = :salarie_id";
        $stmt = $conn->prepare($query);
        $stmt->bindParam(':salarie_id', $salarie_id);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        // Objets
        // Préparation de la requête d'insertion
        $stmt = $conn->prepare("INSERT INTO objet (nom, numero_de_serie, site_internet, adresse_Ip, domaine, processeur, memoire, Disque_Dur, carte_video, windows, lumen, watt, etat, reference, factureA, contrat, date_debut_engagement, date_fin_engagement, type_de_renouvellement, mode_de_paiement, periodicite_de_facturation, date_debut_facturation, date_fin_facturation, type_garantie, date_de_debut_de_garantie, date_de_fin_de_garantie, prix_Ht_avec_garantie, prix_Ttc_avec_garantie, prix_Ht_sans_garantie, prix_Ttc_sans_garantie, prix_Ht_garantie, prix_Ttc_garantie, service_localisation_id, salarie_id,typeObjet_id,projets_id,marque_id)
        VALUES (:nom, :numero_serie, :site_internet, :adresse_Ip, :domaine, :processeur, :memoire, :Disque_Dur, :carte_video, :windows, :lumen, :watt, :etat, :reference, :factureA, :contrat, :date_debut_engagement, :date_fin_engagement, :type_de_renouvellement, :mode_de_paiement, :periodicite_de_facturation, :date_debut_facturation, :date_fin_facturation, :type_garantie, :date_de_debut_de_garantie, :date_de_fin_de_garantie, :prix_Ht_avec_garantie, :prix_Ttc_avec_garantie, :prix_Ht_sans_garantie, :prix_Ttc_sans_garantie, :prix_Ht_garantie, :prix_Ttc_garantie, :service_localisation_id, :salarie_id,:typeObjet_id,:projets_id,:marque_id)");
        $stmt->bindParam(':nom', $nom);
        $stmt->bindParam(':numero_serie', $numero_serie);
        $stmt->bindParam(':site_internet', $site_internet);
        $stmt->bindParam(':adresse_Ip', $adresse_Ip);
        $stmt->bindParam(':domaine', $domaine);
        $stmt->bindParam(':processeur', $processeur);
        $stmt->bindParam(':memoire', $memoire);
        $stmt->bindParam(':Disque_Dur', $Disque_Dur);
        $stmt->bindParam(':carte_video', $carte_video);
        $stmt->bindParam(':windows', $windows);
        $stmt->bindParam(':lumen', $lumen);
        $stmt->bindParam(':watt', $watt);
        $stmt->bindParam(':etat', $etat);
        $stmt->bindParam(':reference', $reference);
        $stmt->bindParam(':factureA', $factureA);
        $stmt->bindParam(':contrat', $contrat);
        $stmt->bindParam(':date_debut_engagement', $date_debut_engagement);
        $stmt->bindParam(':date_fin_engagement', $date_fin_engagement);
        $stmt->bindParam(':type_de_renouvellement', $type_de_renouvellement);
        $stmt->bindParam(':mode_de_paiement', $mode_de_paiement);
        $stmt->bindParam(':periodicite_de_facturation', $periodicite_de_facturation);
        $stmt->bindParam(':date_debut_facturation', $date_debut_facturation);
        $stmt->bindParam(':date_fin_facturation', $date_fin_facturation);
        $stmt->bindParam(':type_garantie', $type_garantie);
        $stmt->bindParam(':date_de_debut_de_garantie', $date_de_debut_de_garantie);
        $stmt->bindParam(':date_de_fin_de_garantie', $date_de_fin_de_garantie);
        $stmt->bindParam(':prix_Ht_avec_garantie', $prix_Ht_avec_garantie);
        $stmt->bindParam(':prix_Ttc_avec_garantie', $prix_Ttc_avec_garantie);
        $stmt->bindParam(':prix_Ht_sans_garantie', $prix_Ht_sans_garantie);
        $stmt->bindParam(':prix_Ttc_sans_garantie', $prix_Ttc_sans_garantie);
        $stmt->bindParam(':prix_Ht_garantie', $prix_Ht_garantie);
        $stmt->bindParam(':prix_Ttc_garantie', $prix_Ttc_garantie);
        $stmt->bindParam(':service_localisation_id', $service_localisation_id);
        $stmt->bindParam(':salarie_id', $salarie_id);
        $stmt->bindParam(':typeObjet_id', $typeObjet_id);
        $stmt->bindParam(':projets_id', $projets_id);
        $stmt->bindParam(':marque_id', $marque_id);
        $stmt->execute();

        // Récupérez l'ID de l'objet inséré
        $objet_id = $conn->lastInsertId();

        // Préparez la requête d'insertion du problème
        $stmt = $conn->prepare("INSERT INTO probleme (description, date_probleme, date_de_resolution, resolution, objet_id) VALUES (:description, :date_probleme, :date_de_resolution, :resolution, :objet_id)");

        // Récupération des données du problème
        $descriptions = $_POST['description'];
        $dateProblemes = $_POST['dateProbleme'];
        $datesResolutions = $_POST['dateResolution'];
        $resolutions = $_POST['resolution'];

        // Insérer chaque problème dans la base de données
        foreach ($descriptions as $index => $description) {
            $date_probleme = $dateProblemes[$index];
            $date_de_resolution = $datesResolutions[$index];
            $resolution = $resolutions[$index];
            // Ajoutez ici la logique pour définir la valeur de $objet_id. 

            // Liaison des paramètres
            $stmt->bindParam(':description', $description);
            $stmt->bindParam(':date_probleme', $date_probleme);
            $stmt->bindParam(':date_de_resolution', $date_de_resolution);
            $stmt->bindParam(':resolution', $resolution);
            $stmt->bindParam(':objet_id', $objet_id);

            // Exécution de la requête
            $stmt->execute();
        }

        // Vérifiez si l'objet et les problèmes ont été insérés avec succès
        if ($stmt->rowCount() > 0) {
            echo "L'objet et les problèmes ont été ajoutés avec succès.";
        } else {
            echo "Erreur lors de l'ajout de l'objet et des problèmes.";
        }


        // Préparez la requête d'insertion de la marque

        // test33
        $stmt = $conn->prepare("REPLACE INTO marque (nom_marque) VALUES (:nom_marque)");
        $stmt->bindParam(':nom_marque', $nom_marque);
        // test33

        // Exécutez la requête d'insertion de la marque
        $stmt->execute();

        // Vérifiez si le problème et la marque ont été insérés avec succès
        if ($stmt->rowCount() > 0) {
            echo "Le problème et la marque ont été ajoutés avec succès.";
        } else {
            echo "Erreur lors de l'ajout du problème et de la marque.";
        }

        // Exécution de la requête
        // $stmt->execute();

        // Vérification du succès de l'insertion
        if ($stmt->rowCount() > 0) {
            echo "L'objet a été ajouté avec succès.";
        } else {
            echo "Erreur lors de l'ajout de l'objet.";
        }
    }

    // Type OBJET

    // Type Objet
    // Récupération de l'ID du Type Objet sélectionné
    $typeObjet_id = $_POST["typeObjet"];

    // Requête pour récupérer le nom du salarié
    $query = "SELECT nom FROM typeObjet WHERE id = :typeObjet_id";
    $stmt = $conn->prepare($query);
    $stmt->bindParam(':typeObjet_id', $typeObjet_id);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);


    // Type OBJET



    ?>
    <div class="form-container">

        <form method="POST" action="">
            <label for="nom">Nom :</label><br>
            <input type="text" name="nom" id="nom" required><br>

            <label for="numero_serie">Numéro de série :</label><br>
            <input type="text" name="numero_serie" id="numero_serie" required><br>

            <label for="site_internet">Site Internet :</label><br>
            <input type="text" name="site_internet" id="site_internet" required><br>

            <label for="adresse_Ip">Adresse IP :</label><br>
            <input type="text" id="adresse_Ip" name="adresse_Ip" required><br>

            <!-- Ajoutez ici les autres champs du formulaire -->
            <label for="domaine">Domaine :</label><br>
            <input type="text" id="domaine" name="domaine" required><br>

            <label for="processeur">Processeur :</label><br>
            <input type="text" id="processeur" name="processeur" required><br>

            <label for="memoire">Mémoire :</label><br>
            <input type="text" id="memoire" name="memoire" required><br>

            <label for="Disque_Dur">Disque Dur :</label><br>
            <input type="text" id="Disque_Dur" name="Disque_Dur" required><br>

            <label for="carte_video">Carte Vidéo :</label><br>
            <input type="text" id="carte_video" name="carte_video" required><br>

            <label for="windows">Windows :</label><br>
            <input type="text" id="windows" name="windows" required><br>

            <label for="lumen">Lumen :</label><br>
            <input type="text" id="lumen" name="lumen" required><br>

            <label for="watt">Watt :</label><br>
            <input type="text" id="watt" name="watt" required><br>

            <label for="etat">État :</label><br>
            <input type="text" id="etat" name="etat" required><br>

            <label for="reference">Référence :</label><br>
            <input type="text" id="reference" name="reference" required><br>

            <label for="factureA">Facture à :</label><br>
            <input type="text" id="factureA" name="factureA" required><br>

            <label for="contrat">Contrat :</label><br>
            <input type="checkbox" id="contrat" name="contrat"><br>

            <label for="date_debut_engagement">Date de début d'engagement :</label><br>
            <input type="date" id="date_debut_engagement" name="date_debut_engagement" required><br>

            <label for="date_fin_engagement">Date de fin d'engagement :</label><br>
            <input type="date" id="date_fin_engagement" name="date_fin_engagement" required><br>

            <label for="type_de_renouvellement">Type de renouvellement :</label><br>
            <input type="text" id="type_de_renouvellement" name="type_de_renouvellement" required><br>

            <label for="mode_de_paiement">Mode de paiement :</label><br>
            <input type="text" id="mode_de_paiement" name="mode_de_paiement" required><br>

            <label for="periodicite_de_facturation">Périodicité de facturation :</label><br>
            <input type="text" id="periodicite_de_facturation" name="periodicite_de_facturation" required><br>

            <label for="date_debut_facturation">Date de début de facturation :</label><br>
            <input type="date" id="date_debut_facturation" name="date_debut_facturation" required><br>

            <label for="date_fin_facturation">Date de fin de facturation :</label><br>
            <input type="date" id="date_fin_facturation" name="date_fin_facturation" required><br>


            <label for="type_garantie">Type de garantie :</label><br>
            <input type="text" id="type_garantie" name="type_garantie" required><br>

            <label for="date_de_debut_de_garantie">Date de début de garantie :</label><br>
            <input type="date" id="date_de_debut_de_garantie" name="date_de_debut_de_garantie" required><br>

            <label for="date_de_fin_de_garantie">Date de fin de garantie :</label><br>
            <input type="date" id="date_de_fin_de_garantie" name="date_de_fin_de_garantie" required><br>

            <label for="prix_Ht_avec_garantie">Prix HT avec garantie :</label><br>
            <input type="text" id="prix_Ht_avec_garantie" name="prix_Ht_avec_garantie" required><br>

            <label for="prix_Ttc_avec_garantie">Prix TTC avec garantie :</label><br>
            <input type="text" id="prix_Ttc_avec_garantie" name="prix_Ttc_avec_garantie" required><br>

            <label for="prix_Ht_sans_garantie">Prix HT sans garantie :</label><br>
            <input type="text" id="prix_Ht_sans_garantie" name="prix_Ht_sans_garantie" required><br>

            <label for="prix_Ttc_sans_garantie">Prix TTC sans garantie :</label><br>
            <input type="text" id="prix_Ttc_sans_garantie" name="prix_Ttc_sans_garantie" required><br>

            <label for="prix_Ht_garantie">Prix HT garantie :</label><br>
            <input type="text" id="prix_Ht_garantie" name="prix_Ht_garantie" required><br>

            <label for="prix_Ttc_garantie">Prix TTC garantie :</label><br>
            <input type="text" id="prix_Ttc_garantie" name="prix_Ttc_garantie" required><br>

            <!-- type d'objet -->
            <label for="typeObjet">Type de l'objet :</label><br>
            <select name="typeObjet" id="typeObjet">
                <option value="" disabled selected>-- Sélectionnez un type d'objet --</option>
                <?php
                $result = $conn->query("SELECT id, nom FROM typeObjet");
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['nom'] . "</option>";
                }
                ?>
            </select><br>

            <!-- type d'objet -->


            <!-- Marque -->
            <label for="nom_marque">Marque :</label><br>
            <select name="marque_id" id="nom_marque">
                <option value="" disabled selected>-- Sélectionnez une marque --</option>
                <?php
                $result = $conn->query("SELECT DISTINCT * FROM marque");
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['nom_marque'] . "</option>";
                }
                ?>
            </select><br>

            <!-- Marque -->

            <!-- service localisation -->

            <label for="service_localisation">Service de localisation :</label><br>

            <select name="service_localisation" id="service_localisation" required>
                <option value="" disabled selected>-- Sélectionnez un Service --</option>
                <?php
                // Requête pour récupérer les services de localisation depuis la base de données
                $query = "SELECT id, nom_du_service FROM serviceLocalisation";
                $result = $conn->query($query);

                // Parcourir les résultats de la requête et générer les options du menu déroulant
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $serviceId = $row['id'];
                    if (isset($row['nom_du_service'])) {
                        $serviceNom = $row['nom_du_service'];
                        echo "<option value=\"$serviceId\">$serviceNom</option>";
                    }
                }
                ?>
            </select><br>

            <!-- service localisation -->


            <!-- salarie -->
            <label for="salarie">Salarie :</label><br>

            <select name="salarie" id="salarie">
                <option value="" disabled selected>-- Sélectionnez un salarie --</option>
                <?php
                // Requête pour récupérer les salariés depuis la base de données
                $query = "SELECT id, nom FROM salarie";
                $result = $conn->query($query);

                // Parcourir les résultats de la requête et générer les options du menu déroulant
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $salarieId = $row['id'];
                    $salarieNom = $row['nom'];
                    echo "<option value=\"$salarieId\">$salarieNom</option>";
                }

                ?>
            </select><br>

            <!-- salarie -->

            <!-- Projet test -->

            <label for="nom_projet">Nom du Projet :</label><br>

            <select name="projet" id="projet" required>
                <option value="" disabled selected>-- Sélectionnez un Projet --</option>
                <?php
                // Requête pour récupérer les services de localisation depuis la base de données
                $query = "SELECT id, nom FROM projets";
                $result = $conn->query($query);

                // Parcourir les résultats de la requête et générer les options du menu déroulant
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    $projetId = $row['id'];
                    if (isset($row['nom'])) {
                        $projetNom = $row['nom'];
                        echo "<option value=\"$projetId\">$projetNom</option>";
                    }
                }
                ?>
            </select><br>
            <?php print_r($projetNom) ?>
            <!-- Projet test -->




            <!-- Probleme -->

            <h3>Problèmes :</h3><br>
            <div id="problemeContainer">
                <label for="description">Description du problème :</label><br>
                <textarea name="description[]" placeholder="Description du problème"></textarea><br>

                <label for="dateProbleme">Date du problème :</label><br>
                <input type="date" name="dateProbleme[]" required><br>

                <label for="dateResolution">Date de résolution :</label><br>
                <input type="date" name="dateResolution[]" required><br>

                <label for="resolution">Résolution :</label><br>
                <textarea name="resolution[]" placeholder="Résolution du problème"></textarea><br>
            </div>

            <button type="button" id="ajouterProbleme">Ajouter un autre problème</button><br>



            <input type="submit" value="Ajouter">
        </form>
    </div>
</body>

</html>