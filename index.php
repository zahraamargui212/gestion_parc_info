<?php require_once 'bd.php'; ?>

<!DOCTYPE html>
<html>
<head>
    <title>Recherche d'objet</title>
    <link rel="stylesheet" href="style.css">
    <script>
        function modifierObjet(id) {
            window.location.href = "modifObjet.php?id=" + id;
        }

        function supprimerObjet(id) {
            if (confirm("Êtes-vous sûr de vouloir supprimer cet objet ?")) {
                var xhr = new XMLHttpRequest();
                xhr.open("POST", "supprimerObjet.php", true);
                xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4 && xhr.status === 200) {
                        alert("L'objet a été supprimé avec succès.");
                        window.location.reload();
                    }
                };
                xhr.send("id=" + id);
            }
        }
    </script>
</head>

<body>
    <form method="POST" action="">
        <label for="nom">Nom objet :</label>
        <select name="nom" id="nom">
            <option value="" disabled selected>-- Sélectionnez un objet --</option>
            <?php
            $result = $conn->query("SELECT DISTINCT nom FROM objet");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                echo "<option value='" . $row['nom'] . "'>" . $row['nom'] . "</option>";
            }
            ?>
        </select>

        <label for="typeObjet">Type de l'objet :</label>
        <select name="typeObjet" id="typeObjet">
            <option value="" disabled selected>-- Sélectionnez un type d'objet --</option>
            <?php
            $result = $conn->query("SELECT id, nom FROM typeObjet");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                echo "<option value='" . $row['id'] . "'>" . $row['nom'] . "</option>";
            }
            ?>
        </select>

        <label for="salarie">Propriétaire :</label>
        <select name="salarie" id="salarie">
            <option value="" disabled selected>-- Sélectionnez un salarié --</option>
            <?php
            $result = $conn->query("SELECT id, nom FROM salarie");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                echo "<option value='" . $row['id'] . "'>" . $row['nom'] . "</option>";
            }
            ?>
        </select>

        <label for="serviceLocalisation">Localisation du service :</label>
        <select name="serviceLocalisation" id="serviceLocalisation">
            <option value="" disabled selected>-- Sélectionnez une localisation du service --</option>
            <?php
            $result = $conn->query("SELECT id, nom_du_service FROM serviceLocalisation");
            if ($result !== false) {
                while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                    echo "<option value='" . $row['id'] . "'>" . $row['nom_du_service'] . "</option>";
                }
            } else {
                $errorInfo = $conn->errorInfo();
                echo "Erreur lors de l'exécution de la requête : " . $errorInfo[2];
            }
            ?>
        </select>

        <label for="marque">Marque:</label>
        <select name="marque" id="marque">
            <option value="" disabled selected>-- Sélectionnez une marque --</option>
            <?php
            $result = $conn->query("SELECT id, nom_marque FROM marque");
            while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                echo "<option value='" . $row['id'] . "'>" . $row['nom_marque'] . "</option>";
            }
            ?>
        </select>

        <input type="submit" value="Rechercher">
    </form>

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Récupération des données du formulaire
        $nom = $_POST['nom'] ?? "";
        $typeObjet = $_POST['typeObjet'] ?? "";
        $proprietaire = $_POST['salarie'] ?? "";
        $serviceLocalisation = $_POST['serviceLocalisation'] ?? "";
        $marque = $_POST['marque'] ?? "";

        // Préparation de la requête
        $query = "SELECT o.id, o.nom, o.numero_de_serie, o.site_internet, o.adresse_Ip, o.domaine, 
        o.processeur, o.memoire, o.Disque_Dur, o.carte_video, o.windows, o.lumen, o.watt, o.etat,
        o.reference, o.factureA, o.contrat, o.date_debut_engagement, o.date_fin_engagement, 
        o.type_de_renouvellement, o.mode_de_paiement, o.periodicite_de_facturation, 
        o.date_debut_facturation, o.date_fin_facturation, o.type_garantie, 
        o.date_de_debut_de_garantie, o.date_de_fin_de_garantie, o.prix_Ht_avec_garantie, 
        o.prix_Ttc_avec_garantie, o.prix_Ht_sans_garantie, o.prix_Ttc_sans_garantie,
        o.prix_Ht_garantie, o.prix_Ttc_garantie, o.service_localisation_id, o.salarie_id, 
        o.typeObjet_id, o.projets_id,
        s.nom AS salarie_nom,
        m.nom_marque,
        t.nom AS nom_typeObjet,
        pr.nom AS nom_projet,
        pr.date_debut AS date_debut_projet,
        pr.date_fin AS date_fin_projet,
        sl.nom_du_service AS service_localisation,
        (SELECT GROUP_CONCAT(p.description SEPARATOR '\n\n') FROM probleme p WHERE p.objet_id = o.id) AS descriptions,
        (SELECT GROUP_CONCAT(p.date_probleme SEPARATOR '\n\n') FROM probleme p WHERE p.objet_id = o.id) AS dates_probleme,
        (SELECT GROUP_CONCAT(p.date_de_resolution SEPARATOR '\n\n') FROM probleme p WHERE p.objet_id = o.id) AS dates_resolution,
        (SELECT GROUP_CONCAT(p.resolution SEPARATOR '\n\n') FROM probleme p WHERE p.objet_id = o.id) AS resolutions
        -- (SELECT GROUP_CONCAT(p.description SEPARATOR '<br>') FROM probleme p WHERE p.objet_id = o.id) AS descriptions,
        -- (SELECT GROUP_CONCAT(p.date_probleme SEPARATOR '<br>') FROM probleme p WHERE p.objet_id = o.id) AS dates_probleme,
        -- (SELECT GROUP_CONCAT(p.date_de_resolution SEPARATOR '<br>') FROM probleme p WHERE p.objet_id = o.id) AS dates_resolution,
        -- (SELECT GROUP_CONCAT(p.resolution SEPARATOR '<br>') FROM probleme p WHERE p.objet_id = o.id) AS resolutions
    FROM objet o
    LEFT JOIN salarie s ON o.salarie_id = s.id
    LEFT JOIN serviceLocalisation sl ON o.service_localisation_id = sl.id
    LEFT JOIN marque m ON o.marque_id = m.id
    LEFT JOIN projets pr ON o.projets_id = pr.id
    LEFT JOIN typeObjet t ON o.typeObjet_id = t.id
    WHERE 1=1";

        if (!empty($nom)) {
            $query .= " AND o.nom = :nom";
        }

        if (!empty($typeObjet)) {
            $query .= " AND o.typeObjet_id = :typeObjet";
        }

        if (!empty($proprietaire)) {
            $query .= " AND o.salarie_id = :salarie";
        }

        if (!empty($serviceLocalisation)) {
            $query .= " AND sl.id = :serviceLocalisation";
        }

        if (!empty($marque)) {
            $query .= " AND m.id = :marque";
        }

        $stmt = $conn->prepare($query);

        if (!empty($nom)) {
            $stmt->bindParam(':nom', $nom);
        }

        if (!empty($typeObjet)) {
            $stmt->bindParam(':typeObjet', $typeObjet);
        }

        if (!empty($proprietaire)) {
            $stmt->bindParam(':salarie', $proprietaire);
        }

        if (!empty($serviceLocalisation)) {
            $stmt->bindParam(':serviceLocalisation', $serviceLocalisation);
        }

        if (!empty($marque)) {
            $stmt->bindParam(':marque', $marque);
        }

        $stmt->execute();

        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // echo "$query<br>";
        if ($result !== false) {
            if (count($result) > 0) {
                echo "<table>";
                echo "<tr>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Propriétaire</th>
                        <th>Type d'Objet</th>
                        <th>Nom localisation du service</th>
                        <th>Numéro de série</th>
                        <th>Site Internet</th>
                        <th>Adresse IP</th>
                        <th>Domaine</th>
                        <th>Processeur</th>
                        <th>Mémoire</th>
                        <th>Disque Dur</th>
                        <th>Carte vidéo</th>
                        <th>Windows</th>
                        <th>Lumen</th>
                        <th>Watt</th>
                        <th>Etat</th>
                        <th>Référence</th>
                        <th>FactureA</th>
                        <th>Contrat</th>
                        <th>Date de début d'engagement</th>
                        <th>Date de fin d'engagement</th>
                        <th>Type de renouvellement</th>
                        <th>Mode de paiement</th>
                        <th>Périodicité de facturation</th>
                        <th>Date de début de facturation</th>
                        <th>Date de fin de facturation</th>
                        <th>Type garantie</th>
                        <th>Date de début de garantie</th>
                        <th>Date de fin de garantie</th>
                        <th>Prix HT avec garantie</th>
                        <th>Prix TTC avec garantie</th>
                        <th>Prix HT sans garantie</th>
                        <th>Prix TTC sans garantie</th>
                        <th>Prix HT garantie</th>
                        <th>Prix TTC garantie</th>
                        <th>Description Problème</th>
                        <th>Date Problème</th>
                        <th>Date de résolution</th>
                        <th>Résolution</th>
                        <th>Marque</th>
                        <th>Nom de Projet</th>
                        <th>Date de début</th>
                        <th>Date de fin</th>
                        <th>Actions</th>
                    </tr>";
                foreach ($result as $row) {
                    echo "<tr>";
                    echo "<td>" . htmlspecialchars($row['id']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['nom']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['salarie_nom']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['nom_typeObjet']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['service_localisation']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['numero_de_serie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['site_internet']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['adresse_Ip']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['domaine']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['processeur']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['memoire']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['Disque_Dur']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['carte_video']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['windows']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['lumen']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['watt']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['etat']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['reference']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['factureA']) . "</td>";
                    echo "<td>" . ($row['contrat'] ? 'Oui' : 'Non') . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_debut_engagement']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_fin_engagement']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['type_de_renouvellement']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['mode_de_paiement']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['periodicite_de_facturation']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_debut_facturation']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_fin_facturation']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['type_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_de_debut_de_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_de_fin_de_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ht_avec_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ttc_avec_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ht_sans_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ttc_sans_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ht_garantie']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['prix_Ttc_garantie']) . "</td>";

                    echo "<td>" . nl2br(htmlspecialchars($row['descriptions'])) . "</td>";
                    echo "<td>" . nl2br(htmlspecialchars($row['dates_probleme'])) . "</td>";
                    echo "<td>" . nl2br(htmlspecialchars($row['dates_resolution'])) . "</td>";
                    echo "<td>" . nl2br(htmlspecialchars($row['resolutions'])) . "</td>";
                    
                    echo "<td>" . htmlspecialchars($row['nom_marque']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['nom_projet']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_debut_projet']) . "</td>";
                    echo "<td>" . htmlspecialchars($row['date_fin_projet']) . "</td>";
                    echo "<td>
                            <button class=\"modify-btn\" onclick=\"modifierObjet(" . $row['id'] . ")\">Modifier</button>
                            <button class=\"delete-btn\" onclick=\"supprimerObjet(" . $row['id'] . ")\">Supprimer</button>
                        </td>";
                    echo "</tr>";
                }
                echo "</table>";
            } else {
                echo "0 résultats";
            }
        } else {
            echo "Erreur lors de l'exécution de la requête";
        }
    }
    ?>

</body>

</html>
