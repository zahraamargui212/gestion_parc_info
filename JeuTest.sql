INSERT INTO marque (nom_marque) VALUES 
('DELL'),
('ThinkPad'),
('ThinkCentre'),
('HP');
INSERT INTO projets (nom, date_debut, date_fin) VALUES 
('Projet 1', '2023-02-01', '2023-12-31'),
('Projet 2', '2023-02-01', '2023-12-31'),
('Projet 3', '2023-03-01', '2023-06-30'),
('Projet 4', '2023-07-01', '2023-07-31');
INSERT INTO salarie (nom) VALUES 
('Pascal'),
('Isabelle'),
('Camille'),
('nina'),
('Florian');
INSERT INTO typeObjet (nom) VALUES 
('Ordinateurs'),
('Imprimantes'),
('Ecrans'),
('Serveurs'),
('Stockages');
INSERT INTO serviceLocalisation (nom_du_service) VALUES 
('Service informatique'),
('Service Evènement et communication'),
('Service Comptabilité'),
('Service accueil');
INSERT INTO salaries_Services_Localisation (id_serviceLocalisation, id_salarie, date_de_debut, date_de_fin) VALUES
(9,2, '2023-02-01', '2024-12-31');
INSERT INTO salaries_Services_Localisation (id_serviceLocalisation, id_salarie, date_de_debut, date_de_fin) VALUES
(9,4, '2023-02-01', '2024-12-31');
INSERT INTO salaries_Services_Localisation (id_serviceLocalisation, id_salarie, date_de_debut, date_de_fin) VALUES
(10,3, '2023-02-01', '2023-12-31');
INSERT INTO salaries_Services_Localisation (id_serviceLocalisation, id_salarie, date_de_debut, date_de_fin) VALUES
(12,1, '2023-02-01', '2023-12-31');
