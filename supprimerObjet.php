<?php
require_once 'bd.php';
?>

<?php
require_once 'bd.php';

// Vérifier si l'ID de l'objet est fourni dans la demande POST
if (isset($_POST['id'])) {
    $id = $_POST['id'];

    // Effectuer la suppression de l'objet dans la base de données
    $stmt = $conn->prepare("DELETE FROM objet WHERE id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    // Envoyer une réponse réussie (statut HTTP 200) pour indiquer que la suppression a été effectuée avec succès
    http_response_code(200);
} else {
    // Envoyer une réponse d'erreur (statut HTTP 400) si aucun ID n'est fourni
    http_response_code(400);
}
?>
