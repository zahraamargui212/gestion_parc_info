<?php
require_once 'bd.php'; // Inclure le fichier de connexion à la base de données

function getObjetById($conn, $id)
{
   
    // Récupérer les informations d'un objet à partir de son ID
//     $stmt = $conn->prepare("SELECT objet.nom, objet.salarie_id, objet.typeObjet_id, objet.service_localisation_id,
//   marque.nom_marque
$stmt = $conn->prepare("SELECT *
  FROM objet
  LEFT JOIN marque ON objet.marque_id = marque.id
  WHERE objet.id = :id");

    $stmt->bindParam(':id', $id);
    $stmt->execute();
    return $stmt->fetch(PDO::FETCH_ASSOC);
}

function getProprietaires($conn)
{
    // Récupérer les propriétaires d'objets depuis la base de données
    $stmt = $conn->prepare("SELECT id, nom FROM salarie");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getTypesObjet($conn)
{
    // Récupérer les types d'objets depuis la base de données
    $stmt = $conn->prepare("SELECT id, nom FROM typeObjet");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getProjets($conn)
{
    $stmt = $conn->prepare("SELECT id, nom,date_debut,date_fin  FROM projets");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


function getServicesLocalisation($conn)
{
    // Récupérer les services de localisation depuis la base de données
    $stmt = $conn->prepare("SELECT id, nom_du_service FROM serviceLocalisation");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function getMarques($conn)
{
    // Récupérer les marques depuis la base de données
    $stmt = $conn->prepare("SELECT id, nom_marque FROM marque");
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

function updateObjet(
    $conn,
    $id,
    $nouveauNom,
    $nouveauProprietaire,
    $nouveauTypeObjet,
    $nouveauService,
    $nouvelleMarque,
    $nouveauNumeroSerie,
    $nouveauSiteInternet,
    $nouvelleAdresseIp,
    $nouveauDomaine,
    $nouveauProcesseur,
    $nouvelleMemoire,
    $nouveauDisqueDur,
    $nouvelleCarteVideo,
    $nouveauWindows,
    $nouveauLumen,
    $nouveauWatt,
    $nouvelEtat,
    $nouvelleReference,
    $nouvelleFactureA,
    $nouveauContrat,
    $nouvelleDateDebutEngagement,
    $nouvelleDateFinEngagement,
    $nouveauTypeRenouvellement,
    $nouveauModePaiement,
    $nouvellePeriodiciteFacturation,
    $nouvelleDateDebutFacturation,
    $nouvelleDateFinFacturation,
    $nouveauTypeGarantie,
    $nouvelleDateDebutGarantie,
    $nouvelleDateFinGarantie,
    $nouveauPrixHtAvecGarantie,
    $nouveauPrixTtcAvecGarantie,
    $nouveauPrixHtSansGarantie,
    $nouveauPrixTtcSansGarantie,
    $nouveauPrixHtGarantie,
    $nouveauPrixTtcGarantie,
    $nouveauProjet
) {
    // Mettre à jour les informations de l'objet dans la base de données
    $stmt = $conn->prepare("UPDATE objet SET nom = :nom, salarie_id = :salarie_id, typeObjet_id = :typeObjet_id, 
    service_localisation_id = :service_localisation_id,  numero_de_serie = :numero_de_serie, 
    site_internet = :site_internet, adresse_Ip = :adresse_Ip, domaine = :domaine, 
    processeur = :processeur, memoire = :memoire, Disque_Dur = :Disque_Dur, carte_video = :carte_video, 
    windows = :windows, lumen = :lumen, watt = :watt, etat = :etat, reference = :reference, factureA = :factureA, 
    contrat = :contrat, date_debut_engagement = :date_debut_engagement, date_fin_engagement = :date_fin_engagement, 
    type_de_renouvellement = :type_de_renouvellement, mode_de_paiement = :mode_de_paiement, 
    periodicite_de_facturation = :periodicite_de_facturation, date_debut_facturation = :date_debut_facturation,
     date_fin_facturation = :date_fin_facturation, type_garantie = :type_garantie, 
     date_de_debut_de_garantie = :date_de_debut_de_garantie, date_de_fin_de_garantie = :date_de_fin_de_garantie, 
     prix_Ht_avec_garantie = :prix_Ht_avec_garantie, prix_Ttc_avec_garantie = :prix_Ttc_avec_garantie, 
     prix_Ht_sans_garantie = :prix_Ht_sans_garantie, prix_Ttc_sans_garantie = :prix_Ttc_sans_garantie, 
     prix_Ht_garantie = :prix_Ht_garantie, prix_Ttc_garantie = :prix_Ttc_garantie, 
     projets_id = :projets_id , marque_id = :marque_id WHERE id = :id");


    $stmt->bindParam(':nom', $nouveauNom);
    $stmt->bindParam(':salarie_id', $nouveauProprietaire);
    $stmt->bindParam(':typeObjet_id', $nouveauTypeObjet);
    $stmt->bindParam(':service_localisation_id', $nouveauService);
    $stmt->bindParam(':numero_de_serie', $nouveauNumeroSerie);
    $stmt->bindParam(':site_internet', $nouveauSiteInternet);
    $stmt->bindParam(':adresse_Ip', $nouvelleAdresseIp);
    $stmt->bindParam(':domaine', $nouveauDomaine);
    $stmt->bindParam(':processeur', $nouveauProcesseur);
    $stmt->bindParam(':memoire', $nouvelleMemoire);
    $stmt->bindParam(':Disque_Dur', $nouveauDisqueDur);
    $stmt->bindParam(':carte_video', $nouvelleCarteVideo);
    $stmt->bindParam(':windows', $nouveauWindows);
    $stmt->bindParam(':lumen', $nouveauLumen);
    $stmt->bindParam(':watt', $nouveauWatt);
    $stmt->bindParam(':etat', $nouvelEtat);
    $stmt->bindParam(':reference', $nouvelleReference);
    $stmt->bindParam(':factureA', $nouvelleFactureA);
    $stmt->bindParam(':contrat', $nouveauContrat);
    $stmt->bindParam(':date_debut_engagement', $nouvelleDateDebutEngagement);
    $stmt->bindParam(':date_fin_engagement', $nouvelleDateFinEngagement);
    $stmt->bindParam(':type_de_renouvellement', $nouveauTypeRenouvellement);
    $stmt->bindParam(':mode_de_paiement', $nouveauModePaiement);
    $stmt->bindParam(':periodicite_de_facturation', $nouvellePeriodiciteFacturation);
    $stmt->bindParam(':date_debut_facturation', $nouvelleDateDebutFacturation);
    $stmt->bindParam(':date_fin_facturation', $nouvelleDateFinFacturation);
    $stmt->bindParam(':type_garantie', $nouveauTypeGarantie);
    $stmt->bindParam(':date_de_debut_de_garantie', $nouvelleDateDebutGarantie);
    $stmt->bindParam(':date_de_fin_de_garantie', $nouvelleDateFinGarantie);
    $stmt->bindParam(':prix_Ht_avec_garantie', $nouveauPrixHtAvecGarantie);
    $stmt->bindParam(':prix_Ttc_avec_garantie', $nouveauPrixTtcAvecGarantie);
    $stmt->bindParam(':prix_Ht_sans_garantie', $nouveauPrixHtSansGarantie);
    $stmt->bindParam(':prix_Ttc_sans_garantie', $nouveauPrixTtcSansGarantie);
    $stmt->bindParam(':prix_Ht_garantie', $nouveauPrixHtGarantie);
    $stmt->bindParam(':prix_Ttc_garantie', $nouveauPrixTtcGarantie);
    $stmt->bindParam(':projets_id', $nouveauProjet);
    $stmt->bindParam(':id', $id);
    $stmt->bindParam(':marque_id', $nouvelleMarque);
    $stmt->execute();

    // // Mettre à jour la marque de l'objet
    // $stmtMarque = $conn->prepare("UPDATE marque SET objet_id = :objet_id WHERE id = :marque_id");
    // $stmtMarque->bindParam(':objet_id', $id);
    // $stmtMarque->execute();
}



// Vérifier si l'ID de l'objet est fourni dans l'URL
if (isset($_GET['id'])) {
    $id = $_GET['id'];

    // Récupérer les informations actuelles de l'objet à partir de la base de données
    $donneesObjet = getObjetById($conn, $id);

    // Vérifier si l'objet existe avec l'ID spécifié
    if (!$donneesObjet) {
        // Rediriger vers la page de recherche d'objets si aucun objet n'est trouvé
        header('Location: index.php');
        exit();
    }

    // Récupérer les propriétaires d'objets depuis la base de données
    $proprietaires = getProprietaires($conn);

    // Récupérer les types d'objets depuis la base de données
    $typesObjet = getTypesObjet($conn);

    $projets = getProjets($conn);

    // Récupérer les services de localisation depuis la base de données
    $servicesLocalisation = getServicesLocalisation($conn);

    // Récupérer les marques depuis la base de données
    $marques = getMarques($conn);

    // Traitement du formulaire de modification
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // Affiche le contenu du tableau POST
        print_r($_POST);

        if (isset($_POST['projets_id'])) {
            $projets_id = $_POST['projets_id'];
        } else {
            // Clé "projets_id" non trouvée dans le tableau POST
            echo "Clé 'projets_id' introuvable dans le tableau POST";
        }

        if (isset($_POST['marque_id'])) {
            $marque_id = $_POST['marque_id'];
        } else {
            // Clé "marque_id" non trouvée dans le tableau POST
            echo "Clé 'marque_id' introuvable dans le tableau POST";
        }
        ///////////////test/////////////////////
        $nouveauNumeroSerie = $_POST['numero_de_serie'] ?? '';
        $nouveauSiteInternet = $_POST['site_internet'] ?? '';
        $nouvelleAdresseIp = $_POST['adresse_ip'] ?? '';
        $nouveauDomaine = $_POST['domaine'] ?? '';
        $nouveauProcesseur = $_POST['processeur'] ?? '';
        $nouvelleMemoire = $_POST['memoire'] ?? '';
        $nouveauDisqueDur = $_POST['disque_dur'] ?? '';
        $nouvelleCarteVideo = $_POST['carte_video'] ?? '';
        $nouveauWindows = $_POST['windows'] ?? '';
        $nouveauLumen = $_POST['lumen'] ?? '';
        $nouveauWatt = $_POST['watt'] ?? '';
        $nouvelEtat = $_POST['etat'] ?? '';
        $nouvelleReference = $_POST['reference'] ?? '';
        $nouvelleFactureA = $_POST['facture_a'] ?? '';
        $nouveauContrat = $_POST['contrat'] ?? '';
        $nouvelleDateDebutEngagement = $_POST['date_debut_engagement'] ?? '';
        $nouvelleDateFinEngagement = $_POST['date_fin_engagement'] ?? '';
        $nouveauTypeRenouvellement = $_POST['type_renouvellement'] ?? '';
        $nouveauModePaiement = $_POST['mode_paiement'] ?? '';
        $nouvellePeriodiciteFacturation = $_POST['periodicite_facturation'] ?? '';
        $nouvelleDateDebutFacturation = $_POST['date_debut_facturation'] ?? '';
        $nouvelleDateFinFacturation = $_POST['date_fin_facturation'] ?? '';
        $nouveauTypeGarantie = $_POST['type_garantie'] ?? '';
        $nouvelleDateDebutGarantie = $_POST['date_debut_garantie'] ?? '';
        $nouvelleDateFinGarantie = $_POST['date_fin_garantie'] ?? '';
        $nouveauPrixHtAvecGarantie = $_POST['prix_ht_avec_garantie'] ?? '';
        $nouveauPrixTtcAvecGarantie = $_POST['prix_ttc_avec_garantie'] ?? '';
        $nouveauPrixHtSansGarantie = $_POST['prix_ht_sans_garantie'] ?? '';
        $nouveauPrixTtcSansGarantie = $_POST['prix_ttc_sans_garantie'] ?? '';
        $nouveauPrixHtGarantie = $_POST['prix_ht_garantie'] ?? '';
        $nouveauPrixTtcGarantie = $_POST['prix_Ttc_garantie'] ?? '';

        // Utilisez les variables ci-dessus dans votre code pour éviter les avertissements


        ///////////////test///////////////////////////

        $nouveauNom = $_POST['nouveauNom'];
        $nouveauProprietaire = $_POST['nouveauProprietaire'];
        $nouveauTypeObjet = $_POST['nouveauTypeObjet'];
        $nouveauProjet = $_POST['projets_id'];
        $nouveauService = $_POST['nouveauService'];
        $nouvelleMarque = $_POST['marque_id'];


        // Validation des données du formulaire (exemple : champ nom non vide)
        if (!empty($nouveauNom)) {
            // Effectuer la mise à jour de l'objet dans la base de données
            updateObjet(
                $conn,
                $id,
                $nouveauNom,
                $nouveauProprietaire,
                $nouveauTypeObjet,
                $nouveauService,
                $nouvelleMarque,
                $nouveauNumeroSerie,
                $nouveauSiteInternet,
                $nouvelleAdresseIp,
                $nouveauDomaine,
                $nouveauProcesseur,
                $nouvelleMemoire,
                $nouveauDisqueDur,
                $nouvelleCarteVideo,
                $nouveauWindows,
                $nouveauLumen,
                $nouveauWatt,
                $nouvelEtat,
                $nouvelleReference,
                $nouvelleFactureA,
                $nouveauContrat,
                $nouvelleDateDebutEngagement,
                $nouvelleDateFinEngagement,
                $nouveauTypeRenouvellement,
                $nouveauModePaiement,
                $nouvellePeriodiciteFacturation,
                $nouvelleDateDebutFacturation,
                $nouvelleDateFinFacturation,
                $nouveauTypeGarantie,
                $nouvelleDateDebutGarantie,
                $nouvelleDateFinGarantie,
                $nouveauPrixHtAvecGarantie,
                $nouveauPrixTtcAvecGarantie,
                $nouveauPrixHtSansGarantie,
                $nouveauPrixTtcSansGarantie,
                $nouveauPrixHtGarantie,
                $nouveauPrixTtcGarantie,
                $nouveauProjet, 
                $nouvelleMarque
            );

            // Rediriger vers la page de recherche d'objets après la modification
            header('Location: index.php');
            exit();
        } else {
            $erreur = "Le champ nom est obligatoire.";
        }
    }
    $nomActuel = htmlspecialchars($donneesObjet['nom']);
    $proprietaireActuel = $donneesObjet['salarie_id'];
    $typeObjetActuel = $donneesObjet['typeObjet_id'];
    $projetActuel = $donneesObjet['projets_id'];
    $serviceLocalisationActuel = $donneesObjet['service_localisation_id'];
    $marqueActuelle = $donneesObjet['marque_id'];
} else {
    // Rediriger vers la page de recherche d'objets si aucun ID n'est fourni
    header('Location: index.php');
    exit();
}
?>

<!DOCTYPE html>
<html>

<head>
    <title>Modifier l'objet</title>
    <link rel="stylesheet" href="formDeSaisie.css">
</head>

<body>
    <h1>Modifier l'objet</h1>

    <form method="POST" action="">
        <?php if (isset($erreur)) { ?>
            <div class="erreur"><?php echo $erreur; ?></div>
        <?php } ?>

        <!-- Champ pour le nouveau nom de l'objet -->
        <label for="nouveauNom">Nouveau nom :</label>
        <input type="text" name="nouveauNom" id="nouveauNom" value="<?php echo htmlspecialchars($nomActuel); ?>" required>


        <!-- Champ pour le nouveau propriétaire de l'objet -->
        <label for="nouveauProprietaire">Nouveau propriétaire :</label>
        <select name="nouveauProprietaire" id="nouveauProprietaire" required>
            <?php foreach ($proprietaires as $proprietaire) { ?>
                <option value="<?php echo $proprietaire['id']; ?>" <?php echo ($proprietaire['id'] == $proprietaireActuel) ? 'selected' : ''; ?>>
                    <?php echo htmlspecialchars($proprietaire['nom']); ?></option>
            <?php } ?>
        </select>

        <!-- Champ pour le nouveau type d'objet -->
        <label for="nouveauTypeObjet">Nouveau type d'objet :</label>
        <select name="nouveauTypeObjet" id="nouveauTypeObjet" required>
            <?php foreach ($typesObjet as $typeObjet) { ?>
                <option value="<?php echo $typeObjet['id']; ?>" <?php echo ($typeObjet['id'] == $typeObjetActuel) ?
                                                                    'selected' : ''; ?>>
                    <?php echo htmlspecialchars($typeObjet['nom']); ?></option>
            <?php } ?>
        </select>

        <label for="nouveauProjet">Projet associé :</label>
        <select id="nouveauProjet" name="projets_id" required>

            <?php foreach ($projets as $projet) { ?>
                <option value="<?php echo $projet['id']; ?>" <?php echo ($projet['id'] == $projetActuel) ?
                                                                    'selected' : ''; ?>>
                    <?php echo htmlspecialchars($projet['nom']); ?></option>
            <?php } ?>

        </select>

        <!-- Champ pour le nouveau service de localisation -->
        <label for="nouveauService">Nouveau service de localisation :</label>
        <select name="nouveauService" id="nouveauService" required>
            <?php foreach ($servicesLocalisation as $service) { ?>
                <option value="<?php echo $service['id']; ?>" <?php echo ($service['id'] == $serviceLocalisationActuel) ? 'selected' : ''; ?>>
                    <?php echo htmlspecialchars($service['nom_du_service']); ?></option>
            <?php } ?>
        </select>

        <!-- Champ pour la nouvelle marque de l'objet -->
        <label for="nouvelleMarque">Nouvelle marque :</label>
        <select name="marque_id" id="nouvelleMarque" required>
            <?php foreach ($marques as $marque) { ?>
                <option value="<?php echo $marque['id']; ?>" <?php echo ($marque['id'] == $marqueActuelle) ? 'selected' : ''; ?>>
                    <?php echo htmlspecialchars($marque['nom_marque']); ?></option>
            <?php } ?>
        </select>
                

        <label for="numero_de_serie">Numéro de série :</label>
        <input type="text" id="numero_de_serie" name="numero_de_serie" value="<?php echo $donneesObjet['numero_de_serie']; ?>">

        <label for="site_internet">Site internet :</label>
        <input type="text" id="site_internet" name="site_internet" value="<?php echo htmlspecialchars($donneesObjet['site_internet']); ?>">

        <label for="adresse_ip">Adresse IP :</label>
        <input type="text" id="adresse_ip" name="adresse_ip" value="<?php echo htmlspecialchars($donneesObjet['adresse_Ip']); ?>">

        <label for="domaine">Domaine :</label>
        <input type="text" id="domaine" name="domaine" value="<?php echo htmlspecialchars($donneesObjet['domaine']); ?>">

        <label for="processeur">Processeur :</label>
        <input type="text" id="processeur" name="processeur" value="<?php echo htmlspecialchars($donneesObjet['processeur']); ?>">

        <label for="memoire">Mémoire :</label>
        <input type="text" id="memoire" name="memoire" value="<?php echo htmlspecialchars($donneesObjet['memoire']); ?>">

        <label for="disque_dur">Disque dur :</label>
        <input type="text" id="disque_dur" name="disque_dur" value="<?php echo htmlspecialchars($donneesObjet['Disque_Dur']); ?>">

        <label for="carte_video">Carte vidéo :</label>
        <input type="text" id="carte_video" name="carte_video" value="<?php echo htmlspecialchars($donneesObjet['carte_video']); ?>">

        <label for="windows">Windows :</label>
        <input type="text" id="windows" name="windows" value="<?php echo htmlspecialchars($donneesObjet['windows']); ?>">

        <label for="lumen">Lumen :</label>
        <input type="text" id="lumen" name="lumen" value="<?php echo htmlspecialchars($donneesObjet['lumen']); ?>">

        <label for="watt">Watt :</label>
        <input type="text" id="watt" name="watt" value="<?php echo htmlspecialchars($donneesObjet['watt']); ?>">

        <label for="etat">État :</label>
        <select id="etat" name="etat" required>
            <option value="Nouveau" <?php if ($donneesObjet['etat'] == 'Nouveau') echo 'selected'; ?>>Nouveau</option>
            <option value="Occasion" <?php if ($donneesObjet['etat'] == 'Occasion') echo 'selected'; ?>>Occasion</option>
            <option value="Réparé" <?php if ($donneesObjet['etat'] == 'Réparé') echo 'selected'; ?>>Réparé</option>
        </select>

        <label for="reference">Référence :</label>
        <input type="text" id="reference" name="reference" value="<?php echo htmlspecialchars($donneesObjet['reference']); ?>">

        <label for="factureA">Facturé à :</label>
        <input type="text" id="factureA" name="factureA" value="<?php echo htmlspecialchars($donneesObjet['factureA']); ?>"><br>

        <label for="contrat">Contrat :</label><br><br>
        <label for=""> OUI</label><br>
        <input type="radio" id="contrat" name="contrat" value="1" <?php if ($donneesObjet['contrat'] == '1') echo 'checked'; ?>><br><br>
        <label for=""> NON</label><br>
        <input type="radio" id="contrat2" name="contrat" value="0" <?php if ($donneesObjet['contrat'] == '0') echo 'checked'; ?>><br><br>

        <label for="date_debut_engagement">Date de début d'engagement :</label>
        <input type="date" id="date_debut_engagement" name="date_debut_engagement" value="<?php echo htmlspecialchars($donneesObjet['date_debut_engagement']); ?>">

        <label for="date_fin_engagement">Date de fin d'engagement :</label>
        <input type="date" id="date_fin_engagement" name="date_fin_engagement" value="<?php echo htmlspecialchars($donneesObjet['date_fin_engagement']); ?>">

        <label for="type_renouvellement">Type de renouvellement :</label>
        <input type="text" id="type_renouvellement" name="type_renouvellement" value="<?php echo htmlspecialchars($donneesObjet['type_de_renouvellement']); ?>">

        <label for="mode_paiement">Mode de paiement :</label>
        <input type="text" id="mode_paiement" name="mode_paiement" value="<?php echo htmlspecialchars($donneesObjet['mode_de_paiement']); ?>">

        <label for="periodicite_facturation">Périodicité de facturation :</label>
        <input type="text" id="periodicite_facturation" name="periodicite_facturation" value="<?php echo htmlspecialchars($donneesObjet['periodicite_de_facturation']); ?>">

        <label for="date_debut_facturation">Date de début de facturation :</label>
        <input type="date" id="date_debut_facturation" name="date_debut_facturation" value="<?php echo htmlspecialchars($donneesObjet['date_debut_facturation']); ?>">

        <label for="date_fin_facturation">Date de fin de facturation :</label>
        <input type="date" id="date_fin_facturation" name="date_fin_facturation" value="<?php echo htmlspecialchars($donneesObjet['date_fin_facturation']); ?>">

        <label for="type_garantie">Type de garantie :</label>
        <input type="text" id="type_garantie" name="type_garantie" value="<?php echo htmlspecialchars($donneesObjet['type_garantie']); ?>">

        <label for="date_debut_garantie">Date de début de garantie :</label>
        <input type="date" id="date_debut_garantie" name="date_debut_garantie" value="<?php echo htmlspecialchars($donneesObjet['date_de_debut_de_garantie']); ?>">

        <label for="date_fin_garantie">Date de fin de garantie :</label>
        <input type="date" id="date_fin_garantie" name="date_fin_garantie" value="<?php echo htmlspecialchars($donneesObjet['date_de_fin_de_garantie']); ?>">

        <label for="prix_ht_avec_garantie">Prix HT avec garantie :</label>
        <input type="text" id="prix_ht_avec_garantie" name="prix_ht_avec_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ht_avec_garantie']); ?>">

        <label for="prix_ttc_avec_garantie">Prix TTC avec garantie :</label>
        <input type="text" id="prix_ttc_avec_garantie" name="prix_ttc_avec_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ttc_avec_garantie']); ?>">

        <label for="prix_ht_sans_garantie">Prix HT sans garantie :</label>
        <input type="text" id="prix_ht_sans_garantie" name="prix_ht_sans_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ht_sans_garantie']); ?>">

        <label for="prix_ttc_sans_garantie">Prix TTC sans garantie :</label>
        <input type="text" id="prix_ttc_sans_garantie" name="prix_ttc_sans_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ttc_sans_garantie']); ?>">

        <label for="prix_ht_garantie">Prix HT garantie :</label>
        <input type="text" id="prix_ht_garantie" name="prix_ht_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ht_garantie']); ?>">

        <label for="prix_Ttc_garantie">Prix TTC garantie :</label>
        <input type="text" id="prix_Ttc_garantie" name="prix_Ttc_garantie" value="<?php echo htmlspecialchars($donneesObjet['prix_Ttc_garantie']); ?>">




        <input type="submit" value="Enregistrer les modifications">
    </form>
</body>

</html>