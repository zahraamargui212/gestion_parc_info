create database IF NOT EXISTS gestionParcInfo;

CREATE USER
    IF NOT EXISTS 'gestionParcInfo' @'localhost' IDENTIFIED BY 'pass_gestionParcInfo';

GRANT
    ALL PRIVILEGES ON gestionParcInfo.* TO 'gestionParcInfo' @'localhost';

FLUSH PRIVILEGES;

use gestionParcInfo;

CREATE TABLE
    IF NOT EXISTS serviceLocalisation (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom_du_service VARCHAR(255) 
    );

CREATE TABLE
    IF NOT EXISTS typeObjet (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255) 
    );

CREATE TABLE
    IF NOT EXISTS salarie (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255) 
    );

CREATE TABLE
    IF NOT EXISTS fournisseur (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255),
        telephone VARCHAR(20),
        e_mail VARCHAR(255),
        contact_commercial VARCHAR(255),
        contact_administratif VARCHAR(255),
        contract_technique VARCHAR(255),
        support_technique VARCHAR(255)
    );

CREATE TABLE
    IF NOT EXISTS projets (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255),
        date_debut DATE,
        date_fin DATE
    );
    CREATE TABLE
    IF NOT EXISTS marque (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom_marque VARCHAR(255) UNIQUE
       
    );

CREATE TABLE
    IF NOT EXISTS objet (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255),
        numero_de_serie VARCHAR(255),
        site_internet VARCHAR(255),
        adresse_Ip VARCHAR(255),
        domaine VARCHAR(255),
        processeur VARCHAR(255),
        memoire VARCHAR(255),
        Disque_Dur VARCHAR(255),
        carte_video VARCHAR(255),
        windows VARCHAR(255),
        lumen VARCHAR(255),
        watt VARCHAR(255),
        etat VARCHAR(255),
        reference VARCHAR(255),
        factureA VARCHAR(255),
        contrat BOOLEAN,
        date_debut_engagement date,
        date_fin_engagement date,
        type_de_renouvellement VARCHAR(255),
        mode_de_paiement VARCHAR(255),
        periodicite_de_facturation VARCHAR(255),
        date_debut_facturation date,
        date_fin_facturation date,
        type_garantie VARCHAR(255),
        date_de_debut_de_garantie DATE,
        date_de_fin_de_garantie DATE,
        prix_Ht_avec_garantie DECIMAL(10, 2),
        prix_Ttc_avec_garantie DECIMAL(10, 2),
        prix_Ht_sans_garantie DECIMAL(10, 2),
        prix_Ttc_sans_garantie DECIMAL(10, 2),
        prix_Ht_garantie DECIMAL(10, 2),
        prix_Ttc_garantie DECIMAL(10, 2),
        fournisseur_id INT,
        service_localisation_id INT,
        salarie_id INT,
        projets_id INT,
        typeObjet_id INT,
        marque_id INT,
        FOREIGN KEY (marque_id) REFERENCES marque(id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (fournisseur_id) REFERENCES fournisseur(id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (service_localisation_id) REFERENCES serviceLocalisation(id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (salarie_id) REFERENCES salarie(id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (projets_id) REFERENCES projets(id) ON DELETE CASCADE ON UPDATE CASCADE,
        FOREIGN KEY (typeObjet_id) REFERENCES typeObjet(id) ON DELETE CASCADE ON UPDATE CASCADE
    );

CREATE TABLE
    IF NOT EXISTS probleme (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        description TEXT,
        date_probleme DATE,
        date_de_resolution date,
        resolution TEXT,
        objet_id INT,
        FOREIGN KEY (objet_id) REFERENCES objet(id) ON DELETE CASCADE ON UPDATE CASCADE
    );



CREATE TABLE
    IF NOT EXISTS facture (
        id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        nom VARCHAR(255),
        factureA DECIMAL(10, 2),
        contrat BOOLEAN,
        date_debut_engagement DATE,
        date_fin_engagement DATE,
        type_de_renouvellement VARCHAR(255),
        mode_de_paiement VARCHAR(255)
    );

CREATE TABLE
    IF NOT EXISTS salaries_Services_Localisation (
        id_serviceLocalisation INT(11) NOT NULL,
        id_salarie INT(11) NOT NULL,
        date_de_debut date,
        date_de_fin date,
        Foreign Key (id_serviceLocalisation) REFERENCES serviceLocalisation(id) ON DELETE CASCADE ON UPDATE CASCADE,
        Foreign Key (id_salarie) REFERENCES salarie(id) ON DELETE CASCADE ON UPDATE CASCADE
    );

    